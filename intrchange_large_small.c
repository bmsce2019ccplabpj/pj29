#include <stdio.h>
int main()
{
    int i,n,l=0,s=0;
    printf("Enter the number of elements in an array\n");
    scanf("%d",&n);
    int array[n];
    printf("Enter %d elements of the array\n",n);
    for(i=0; i<n; i++)
    {
       scanf("%d",&array[i]);
    }
    printf("Elements of the array are:\n");
    for(i=0; i<n; i++)
    {
       printf("%d\n",array[i]);
    }
    for(i=0; i<n; i++)
    {
       l=(array[l]>array[i])?l:i;
       s=(array[s]<array[i])?s:i;
    }
    printf("Position of largest value is %d\nPosition of smallest value is %d\n",l+1,s+1);
    printf("Largest value = %d\nSmallest value = %d\n",array[l],array[s]);
    array[l]+=array[s];
    array[s] = array[l]-array[s];
    array[l]-=array[s];
    printf("Elements of the array are:\n");
    for(i=0; i<n; i++)
    {
       printf("%d\n",array[i]);
    }
    return 0;
}